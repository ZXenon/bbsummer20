﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using System;
public class CreateCardPrefabs : MonoBehaviour
{
    //public GameObject prefab;
    // Start is called before the first frame update
    static string[,] s;
    void Awake(){
    }
    [MenuItem ("Card Exporter/Generate Card List")]
    static void Test(){
        string[] lines = File.ReadAllLines("Assets/Scripts/cards.txt");  
        s = new string[lines.Length+1,29];
        
        for (int i = 1; i <lines.Length; i++){
            
            for (int j = 0; j <29; j++){
                //populate the list of strings.
                //print(lines[i].Length);
                //print(lines[j].Length);
                s[i,j] = lines[i].Split('\t')[j];
                //okay im taking a break!
            }
        }
        foreach(string line in s){
            print(line);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    [MenuItem ("Card Exporter/Export Cards")]
    static void DoCreateSimplePrefab()
    {
    //Transform[] transforms = Selection.transforms;
    //foreach (Transform t in transforms) {
        //
        for (int i = 1; i < s.Length;i++){
        //for (int i = 1; i < 3;i++){
        GameObject prefabRef = (GameObject)AssetDatabase.LoadMainAssetAtPath("Assets/Prefabs/Card Template.prefab");
        GameObject instanceRoot = (GameObject)PrefabUtility.InstantiatePrefab(prefabRef);
        GameObject pVariant = PrefabUtility.SaveAsPrefabAsset(instanceRoot, "Assets/Prefabs/Temp/"+s[i,1] + " "+ s[i,0] +".prefab"); //Object prefab = EditorUtility.CreateEmptyPrefab("Assets/Temporary/"+t.gameObject.name+".prefab");
        Card c = pVariant.GetComponent<Card>();
    
        c.IDNumber = s[i,0];
        //print(s[i,1]);
        c.cardName = s[i,1];
        c.cardClass = s[i,2];
            
        if (Int32.TryParse(s[i,3], out int numValue)){
            c.cost = numValue;
        }
        c.cardText = s[i,4];
        if (Int32.TryParse(s[i,5], out int numValue2)){
            c.damage = numValue2;
        }
        if (Int32.TryParse(s[i,6], out int numValue3)){
            c.numAttacks = numValue3;
        }
        if (s[i,7].ToLower() == "true"){
            c.singleTarget = true;
        } else { c.singleTarget = false; }
        if (Int32.TryParse(s[i,8], out int numValue4)){
            c.defense = numValue4;
        }
        if (Int32.TryParse(s[i,9], out int numValue5)){
            c.healing = numValue5;
        }
        if (Int32.TryParse(s[i,10], out int numValue6)){
            c.cardsDrawn = numValue6;
        }
        if (Int32.TryParse(s[i,11], out int numValue7)){
            c.bleed = numValue7;
        }
        if (Int32.TryParse(s[i,12], out int numValue8)){
            c.blessing = numValue8;
        }
        GameObject.DestroyImmediate(instanceRoot);
        }//EditorUtility.ReplacePrefab(t.gameObject, prefab, ReplacePrefabOptions.ConnectToPrefab);
    //}
    }   
}
