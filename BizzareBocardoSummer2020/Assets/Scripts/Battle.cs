﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Battle : MonoBehaviour
{
    // Start is called before the first frame update
    public Goblin goblin;
    public Character player;
    public DisplayManager displayManager;
    public bool playerTurn = true;
    bool enemyActive = false;
    public bool battleFinished = false;
    public bool playerWon = false;
    void Start()
    {
        //this is an example Battle

    }
    
    // Update is called once per frame
    void Update()
    {
        if (playerTurn == false && enemyActive == false && battleFinished == false){
            StartCoroutine(EnemyTurn());
        }   
    }
    IEnumerator EnemyTurn(){
        enemyActive = true;
        goblin.DoEnemyTurn();
        
        yield return new WaitForSeconds(1f);
        goblin.SwitchIntention();
        playerTurn = true;
        enemyActive = false;
        player.PlayerTurnStart();
        StartCoroutine(displayManager.PlayerTurnDisplay());
    }


    

    //card stuff damage stuff
    public void DamagePlayer(int damage){
        while (player.shield >= 1){
            damage -= 1;
            player.shield -= 1;
        }
        if (damage > 0){
            player.currentHealth -= damage;
            if (player.currentHealth < 0){
                player.currentHealth = 0;
                battleFinished = true;
                playerWon = false;
            }
        }
    }
    public void DamageEnemy(int damage){
        while (goblin.shield >= 1){
            damage -= 1;
            goblin.shield -= 1;
        }
        if (damage > 0){
            goblin.currentHealth -= damage;
            if (goblin.currentHealth < 0){
                goblin.currentHealth = 0;
                battleFinished = true;
                playerWon = true;
            }
        }
    }

}
