﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class DisplayManager : MonoBehaviour
{
    // Start is called before the first frame update
    public Text PlayerHpText;
    public Text EnemyHpText;
    public Text BpText;
    public Text ApText;
    public Text TerminalText;
    public Text ActionText;
    public InputField inputField;
    public Button button;
    public Battle battle;
    public Goblin goblin;
    public Character player;
    public bool enemyDisplay = false;
    public bool playerDisplay = false;
    float t;
    void Start()
    {
        StartCoroutine(PlayerTurnDisplay());
        

    }
    public IEnumerator PlayerTurnDisplay()
     {
        playerDisplay = false;
        yield return new WaitForSeconds(.2f);
        playerDisplay = true;
         
         //Your Function You Want to Call
     }

    // Update is called once per frame
    public void EndTurn(){
        StartCoroutine(PlayerTurnWaiting());
    }
    void Update()
    {
        //t += Time.deltaTime;
        if (battle.playerTurn == true && battle.battleFinished != true){
            if (Input.GetKeyDown(KeyCode.KeypadEnter)){
                //InputCard();
            }
            if (Input.GetKeyDown(KeyCode.KeypadPlus)){
                
                //StartCoroutine(PlayerTurnWaiting());
                //ends ur turn
            }
        }
        if (playerDisplay == true){
            PlayerHpText.text = "HP: "+player.currentHealth.ToString() + " /"+ player.maxHealth.ToString();
            EnemyHpText.text = "Enemy HP: "+ goblin.currentHealth.ToString() +"\n BP: "+goblin.shield+"\n"+goblin.publicIntention;
            BpText.text = "BP: "+player.shield.ToString();
            ApText.text = "AP: "+player.currentAP.ToString()+" /" +player.maxAP.ToString();
            TerminalText.text = goblin.name+ " has "+goblin.currentHealth.ToString()+ " hp\n";
            
            //TerminalText.text += t.ToString();
            TerminalText.text += "you have " + player.currentHealth.ToString() + " /"+ player.maxHealth.ToString()+
            " hp   "+ player.shield + " bp         " +player.currentAP.ToString()+" /" +player.maxAP.ToString() +" ap  " +"\n";
            TerminalText.text += "You have the following cards:\n\n";
            for (int i = 0; i < player.hand.Count; i++){
//                TerminalText.text += "a "+ player.hand[i].name + " that costs " + player.hand[i].GetComponent<Card>().cost.ToString()+" AP \n";
            }
        }
        if (enemyDisplay == true){

        }
        //change this to player won.
        if (battle.battleFinished == true){
            if (battle.playerWon == true){
                ActionText.text = "You Win!";
            } else {
                ActionText.text = "You Died!";
            }
            StartCoroutine(BattleWon());
        }
        
        if (Input.GetKeyDown(KeyCode.Escape)){
            SceneManager.LoadScene("BattleUIBlake");
        }
    }
    //this needs to change for the UI.
    void InputCard(){
        //print("Called once?");
        //if (player.hand.contains(inputField.text)){
            //print("it works");
        //}
        for (int i = 0; i <player.hand.Count; i++){
            if (player.hand[i].name == inputField.text){
                //The Player plays their card they chose.
                player.PlayCard(player.hand[i]);
                break;
            }
        }
    }
    public IEnumerator PlayerTurnWaiting(){
        player.EndTurn();
        enemyDisplay = true;
        
        battle.playerTurn = false;
        //ActionText.text = "Card Played; "+ player.lastAction;
        //currently putting this is in the character class for the time being.
        yield return new WaitForSeconds(.6f);
        ActionText.text = "Enemy Moved";
        battle.playerTurn = false;
        enemyDisplay = false;
    }
    IEnumerator BattleWon(){
        
        yield return new WaitForSeconds(1.6f);
        
        SceneManager.LoadScene("BattleUIBlake");
    }
     
}
