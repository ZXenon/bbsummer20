﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
public class DropZone : MonoBehaviour, IDropHandler, IPointerEnterHandler, IPointerExitHandler
{
    //public Draggable.Slot typeOfItem = Slot.WEAPON;
    public Character character;
    public void OnPointerEnter(PointerEventData eventData){
        if (eventData.pointerDrag == null){
            return;
        }
        Draggable d = eventData.pointerDrag.GetComponent<Draggable>();
        if (d != null){
            d.placeholderParent = this.transform;
        }
    }
    public void OnPointerExit(PointerEventData eventData){
        if (eventData.pointerDrag == null){
            return;
        }
        Draggable d = eventData.pointerDrag.GetComponent<Draggable>();
        if (d != null && d.placeholderParent == this.transform){
            d.placeholderParent = d.parentToReturnTo;
        }
    }
    public void OnDrop(PointerEventData eventData){
        Draggable d = eventData.pointerDrag.GetComponent<Draggable>();
        //print(d);
        if (d != null){
            if (this.transform.tag == "Tabletop"){
                if (character.PlayCard(d.gameObject)){
                    //print("Char play");
                    //disappear effect here or in PlayCard u choose.
                } else {
                    //d.parentToReturnTo = GameObject.FindGameObjectWithTag("HandUI").transform;
                    //print("SAVED");
                    //d.GetComponent<CanvasGroup>().blocksRaycasts = true;
                }
            } else {
                //print("SAVED");
                //d.GetComponent<CanvasGroup>().blocksRaycasts = true;
                //d.GetComponent<CanvasGroup>().blocksRaycasts = false;
                d.parentToReturnTo = this.transform;
            }
            
            //print(d.gameObject.name);
            /*
            if card is playable (we have enough actual points) play card. that is where this will go.

            if(typeOfItem == d.typeOfItem){
                d.parentToReturnTo = this.transform;
            }
            */
        }
    }
}
