﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    public GameObject attackExplosion;
    public Deck deck;
    public Battle battle;
    public List<GameObject> hand;
    public Transform handUI;
    public Transform discardPileUI;
    public DisplayManager displayManager;
    public int maxHealth = 15;
    public int currentHealth = 15;
    public int maxAP = 4;
    public int currentAP;
    public int shield  = 0;

    public int bleed;
    public int blessing;
    int startingHandSize = 5;
    public string lastAction = "";
    // Start is called before the first frame update
    void Start()
    {
        PlayerTurnStart();
    }
    public void PlayerTurnStart(){
        currentAP = maxAP;
        shield  = 0;
        for (int i = 0; i < startingHandSize; i++){
            DrawCard();
        }
    }
    public void DrawCard(){
        //lets analyse this.
        hand.Add(deck.CardsInDrawPile[0]);
        GameObject g = Instantiate(deck.CardsInDrawPile[0], new Vector3(0, 0, 0), Quaternion.identity);
        
        g.transform.SetParent(handUI.transform, false);
        deck.CardsInDrawPile.RemoveAt(0);
    }
    public void EndTurn(){
        DiscardHand();
    }
    //
    public bool PlayCard(GameObject c){
        Card card = c.GetComponent<Card>();

        //IMPORTANT NOTE BLAKE: CURRENTLY UR TURNS ENDS WHEN U PLAY A CARD
        //THATS WHY NOW WE ARE DISCARDING THE WHOLE HAND OKAY? THIS WILL CHANGE.
        if (currentAP >= card.cost){
            //plays the card
            GameObject cardEffect = Instantiate(attackExplosion,c.transform.position,Quaternion.identity);
            cardEffect.transform.SetParent(GameObject.FindGameObjectWithTag("Canvas").transform);
            Destroy(cardEffect,3f);
            GameObject cTmp = Instantiate(c);
            cTmp.transform.SetParent(discardPileUI.transform, false);
            cTmp.GetComponent<Draggable>().parentToReturnTo =null;
            cTmp.GetComponent<Draggable>().placeholderParent =null;
            cTmp.GetComponent<CanvasGroup>().blocksRaycasts = true;
            DiscardCard(cTmp);
            card.Play();
            displayManager.ActionText.text = card.whatHappened;
            //displayManager.ActionText.text = "Card Played; "+ lastAction;
            currentAP -= card.cost;
            
            //c.transform.SetParent(discardPileUI.transform, false);
            Destroy(c);
            return true;
        } else {
            return false;
        }
        
    }
    public void DiscardCard(GameObject c){
        // im not sure if remove is acting how i would like.
        
        deck.CardsInDiscard.Insert(0,c);
        hand.Remove(c);
    }
    public void DiscardHand(){
        for (int i = 0; i < hand.Count; i++){
            deck.CardsInDiscard.Insert(0,hand[0]);
        }
        foreach (Transform child in handUI.transform) {
            GameObject.Destroy(child.gameObject);
        }
        
        hand.Clear();
        deck.ShuffleToDrawPile();
        /*
        print("HAND CONTENTS SHOULD BE EMPTY:");
        for (int i = 0; i < hand.Count; i++){
            print(hand[i]);
        }
        print("END");
        print("DrAWPILE CONTENTS SHOULD NOT BE EMPTY:");
        for (int i = 0; i < deck.CardsInDrawPile.Count; i++){
            print(deck.CardsInDrawPile[i]);
        }
        print("END");*/
    }

    
}
