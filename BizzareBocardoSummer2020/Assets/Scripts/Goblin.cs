﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Goblin : MonoBehaviour
{
    
    public int maxHealth = 12;
    public int currentHealth = 5;
    public int shield;

    public int bleed;
    public int blessing;
    public string intention;
    public string publicIntention;
    //temp variables for DoAttack.. etc
    int x;
    int y;
    int z;
    Battle battle;
    //public List<string> intentions = new List<string> {"attacking","blocking","unknown"};
    List<string> intentions = new List<string> {"attacking","blocking","blockattack"};
    // Goblin Information: + terminal prep
    // display health and intention
    // 
    // Player Info:

    //Cards in your hand:

    //Type in a card and press enter.
    void Start()
    {
        battle = GameObject.FindGameObjectWithTag("Battle").GetComponent<Battle>();
        intention = "resting";
        publicIntention = "Resting";
    }
    public void SwitchIntention(){
        shield = 0;
        int r = Random.Range(0,intentions.Count);
        intention = intentions[r];
        DoEnemyIntention();
    }
    void Attack(){
        x = Random.Range(1,5);
        publicIntention = "Attacking for "+x.ToString();
        //does 2 damage
    }
    void DoAttack(int damage){
        battle.DamagePlayer(damage);
    }
    void Block(){
        x = Random.Range(3,8);
        shield = x;
        publicIntention = "Blocking for "+x.ToString();
    }
    void DoBlock(int block){
        //shield = block;
    }
    void BlockAttack(){
        x = Random.Range(1,5);
        y = Random.Range(3,5);
        
        shield = y;
        publicIntention = "Attacking for "+x.ToString()+"\nBlocking for "+y.ToString();
    }
    void DoBlockAttack(int damage, int block){
        battle.DamagePlayer(damage);
        //shield = block;
    }
    public void DoEnemyIntention(){
        if (intention == "attacking"){
            Attack();
        } else if (intention == "blocking"){
            Block();
        } else if (intention == "blockattack"){
            BlockAttack();
        }
    }
    public void DoEnemyTurn(){
        print("do it happen?");
        print(intention);
        if (intention == "attacking"){
            print("Do the attack!" +x);
            DoAttack(x);
        } else if (intention == "blocking"){
            DoBlock(x);
        } else if (intention == "blockattack"){
            DoBlockAttack(x,y);
        }
    }
    
}
