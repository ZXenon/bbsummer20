﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Deck : MonoBehaviour
{
    // 
    //The complete deck of player cards

    //has a list of cards currently in the deck
    //a list of cards in the discard, in order from up most to down most.

    //UI NOTE: I am using just Card scripts and i need GameObjects to make this appear in the UI.
    public List<GameObject> AllCards;
    public List<GameObject> CardsInDrawPile;
    public List<GameObject> CardsInDiscard;
    void Awake()
    {
        //shuffle the deck. probably could be its own function.
        //
        //thoughts: function for shuffle the whole deck; just drawpile could be useful.

        //THIS JUST IN:

        //THE SHUFFLE IS NOT WORKING: I HAVE BEEN PICKING RANDOM CARDS FOR THE ENTIRE LIST.
        //
        List<GameObject> TmpAllCards = new List<GameObject>(AllCards);
        int shuffleLength = TmpAllCards.Count;
        for (int i=0; i < shuffleLength ;i++){
            int r = Random.Range(0,TmpAllCards.Count);
            CardsInDrawPile.Add(TmpAllCards[r]);
            TmpAllCards.RemoveAt(r);
            
        }
        //for (int i=0; i < CardsInDrawPile.Count;i++){
        //    print(CardsInDrawPile[i]);
        //}
    }
    public void ShuffleToDrawPile(){
        int discardLength = CardsInDiscard.Count;
        for (int i=0; i < discardLength;i++){
            CardsInDrawPile.Add(CardsInDiscard[i]);
            
        }
        CardsInDiscard.Clear();
        
        Shuffle(CardsInDrawPile);
        
    }
    void Shuffle(List<GameObject> deckToBeShuffled){
        List<GameObject> TmpAllCards =  new List<GameObject>(deckToBeShuffled);
        
        int shuffleLength = TmpAllCards.Count;
        deckToBeShuffled.Clear();
        for (int i=0; i < shuffleLength;i++){
            int r = Random.Range(0,TmpAllCards.Count);
            deckToBeShuffled.Add(TmpAllCards[r]);
            TmpAllCards.RemoveAt(r);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
