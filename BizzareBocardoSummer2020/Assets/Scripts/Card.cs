﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Card : MonoBehaviour
{
    
    //include variables for face and back images 
    public string IDNumber;
    public string cardName;
    public string cardClass;
    public int cost = 1;
    public string cardText;
    public int damage;
    public int numAttacks;
    public bool singleTarget = true;

    public int defense;
    public int healing;
    public int cardsDrawn;

    public int bleed;
    public int blessing;

    [HideInInspector]   
    public string whatHappened = "";
    Battle battle;
    Character player;
    // Start is called before the first frame update

    public Text cardNameText;
    public Text cardCostText;
    public Text cardTextText;
    public Image cardImage;
    void Awake()
    {
        
        cardNameText.text = cardName;
        cardCostText.text = cost.ToString();
        cardTextText.text = cardText;
    }
    public Card GetCard(){
        return this.GetComponent<Card>();
    }
    // Update is called once per frame
    public void Play(){
        whatHappened = "";
        battle = GameObject.FindGameObjectWithTag("Battle").GetComponent<Battle>();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Character>();
        //eventually these will be replaced with animations
        if (damage > 0){
            
            for (int i = 0; i < numAttacks; i++){
                battle.DamageEnemy(damage);
                whatHappened += "dealt " + damage.ToString() +" damage\n";
            }
            whatHappened += "with "+numAttacks + " number of attacks\n";
        }
        if (defense > 0){
            player.shield += defense;
            whatHappened += "added " + defense.ToString() + " block\n";
        }
        if (cardsDrawn > 0){
            for (int i = 0; i < cardsDrawn; i++){
                player.DrawCard();
            }
            whatHappened += "drew " + cardsDrawn.ToString() + " cards\n";
        }
        if (healing > 0){
            player.currentHealth += healing;
            if (player.currentHealth > player.maxHealth ){
                player.currentHealth = player.maxHealth;
            }
        }

        //do what the card does.
    }
}
